function graba_maestro(){  
//    console.log($("#cliente_n").val());
    var cliente = $("#cliente_n").val();
    var factura = $("#factura_n").val();
    var fecha = $("#fecha").val();
    
    if (cliente != "") {
       
        $.post('procesando.php', {cliente_grabar: cliente,factura_grabar: factura,fecha_grabar:fecha}, function(resultado) {  
           alert(resultado);
         //$("#factura_n").val(resultado);
        

        }); 

    }    
}
function grabando_detalle(){
   
   var producto = $("#nuevo_producto").val();
   var cantidad = $("#nueva_cantidad").val();
   var descuento = $("#nuevo_dto").val();
   var factura_cab = $("#factura_n").val();
    

    if (producto != "" && cantidad != "" && factura_cab != "") {
       
        $.post('procesando.php', {grabarProductoDet: producto,grabarCantidadDet: cantidad,grabarDescuentoDet:descuento,grabarFacturaDet:factura_cab}, function(resultado) {  
            
            $('#nuevo_producto').val(''); 
            $('#nueva_cantidad').val('');
            $('#nuevo_dto').val('');
            $('#datos_detalle').click();  
            
         
        }); 

    }
}  

function mostrar_detalle(){
    var factura = $("#factura_n").val();   
    $.post('procesando.php', {mostrandoDetalle:factura}, function(resultado) {     
        $("#div_datos_detalle").html(resultado);
        var iva = $("tr td")[16].innerHTML;
        var totaldetalle =  $("tr td")[17].innerHTML;
        $("#totaliva").val(iva);
        $("#totaldetalle").val(totaldetalle);
    }); 
}
  


function elimina_maestro(){
    var factura = $("#factura_n").val();
    if (factura != "") {

        $.post('procesando.php', {elimina_maestro: factura}, function(resultado) {  
//            alert(resultado);

        });
    }

}

function eliminar_detalle(id_detalle){
    var id_eliminar = id_detalle;
    var facturada = $("#facturada").val();
    
    if(facturada >0){
       alert("Proforma facturada. Imposible eliminar detalle.");
       exit;
    }
    $.post('procesando.php', {elimina_detalle: id_eliminar}, function(resultado) { 
      
        $('#datos_detalle').click();
    });
}
function editar_detalle(id_detalle,id_prod,cantidad,descuento,precio){
    
    $('#nuevo_producto').css({
        'background' : 'blue',
        'color' : 'white',
        'width' : '150px'
    });
      $('#nueva_cantidad').css({
        'background' : 'blue',
        'color' : 'white',
        'width' : '150px'
    });
    $('#nuevo_dto').css({
        'background' : 'blue',
        'color' : 'white',
        'width' : '150px'
    });
     $('#nuevo_precio').css({
        'background' : 'blue',
        'color' : 'white',
        'width' : '150px'
    });
    
    $('#lbl_precio').show();
    $('#nuevo_precio').show();
    
    $('#grabar_detalle').css('display','none');
    $('#actualizar_detalle').show();
        
           
    $('#nuevo_producto').val(id_prod);
    $('#nuevo_precio').val(precio);
    $('#nueva_cantidad').val(cantidad);
    $('#nuevo_dto').val(descuento);
    $('#id_detalle').val(id_detalle);
}   

function actualizar_detalle(){        
    var nuevo_producto = $('#nuevo_producto').val();
    var id_detalle = $('#id_detalle').val();
    var nueva_cantidad = $('#nueva_cantidad').val();
    var nuevo_dto = $('#nuevo_dto').val();
    var nuevo_precio_actualizar = $('#nuevo_precio').val();
    
  $.post('procesando.php', {actualizar_detalle: nuevo_producto,id_detalle:id_detalle,nueva_cantidad:nueva_cantidad,nuevo_dto:nuevo_dto,nuevo_precio:nuevo_precio_actualizar}, function(resultado) { 
     //alert(resultado);
       $('#datos_detalle').click();
    });  
    
  
    
    
}


function nueva_factura(){
    var fecha = new Date(); //Fecha actual
    var mes = fecha.getMonth()+1; //obteniendo mes
    var dia = fecha.getDate(); //obteniendo dia
    var ano = fecha.getFullYear(); //obteniendo año
    if(dia<10)
     dia='0'+dia; //agrega cero si el menor de 10
    if(mes<10)
     mes='0'+mes //agrega cero si el menor de 10
    $("#fecha").val(ano+"-"+mes+"-"+dia);

 $.post('procesando.php', {nueva_factura:1}, function(resultado) {  

     $("#factura_n").val(resultado);


    }); 
 
}

function buscoDatosCli(){
    var cliente = $("#cliente_n").val();
      
    if (cliente != "") {

       $.post('procesando.php', {mostrar_datos_cli: cliente}, function(resultado) {  
           $("#datos_cliente").html(resultado);
          
       });
       
   }
}

function buscarFactura(){
    var factura_buscar = $("#filtrar_factura").val();
    var albaran_buscar = $("#filtrar_albaran").val();
    
    if((factura_buscar > 0) && (albaran_buscar >0)){
        alert("Solo se podrá filtrar por albarán o por factura");
        exit;
    }
     if(factura_buscar > 0){
        var albaran_factura = factura_buscar;
        var tipo = 'f';
     }
     if(albaran_buscar > 0){
        var albaran_factura = albaran_buscar;
        var tipo = 'a'; 
      }
     
    var array_cuotas = [];
    
//    $("#totaliva").val('');
//    $("#totaldetalle").val('');
//    $("#totalfactura").val('');
    
    $.post('procesando.php', {buscar_factura_maestro: albaran_factura,tipo:tipo}, function(resultado) { 
       
      // alert(JSON.stringify(resultado.cuotasiva[0]));
//         var array_cuotas = JSON.stringify(resultado.cuotasiva);
//         console.log(JSON.stringify(resultado.cuotasiva));
//    
       var facturado = resultado.nfacturacion;
       
        
        
        $("#cliente_n").val(resultado.cliente);
        $("#fecha").val(resultado.fecha);
        
        $("#factura_n").val(resultado.factura);      
        $("#facturada").val(resultado.nfacturacion);
        
        
        if (facturado>0){
//            $('#facturada').css('display','block');  
            $('.deshab').addClass('botones_desh');
//            $('.deshab').attr('disabled','disabled');
            $('.pie').css('display','none');
            $('#fecha').attr('disabled','disabled');
            $('#factura_n').attr('disabled','disabled');
            $('#cliente_n').attr('disabled','disabled');
            $('#factura_negativa').removeClass('botones_desh');
            
        }else{
//            $('#facturada').css('display','none');  
            $('.deshab').removeClass('botones_desh');
            $('.pie').css('display','block');
            $('#fecha').removeAttr('disabled');
            $('#factura_n').removeAttr('disabled');
            $('#cliente_n').removeAttr('disabled');
            $('#factura_negativa').addClass('botones_desh');
        }
      
          
       // alert (Object.keys(resultado.cuotasiva).length);
       $("#totaliva").html('');
        var elementos = Object.keys(resultado.cuotasiva).length;
        var i;
        for (i = 0; i < elementos; i++) {
          // Se ejecuta 5 veces, con valores desde paso desde 0 hasta 4.
       //  alert(resultado.cuotasiva[i]['importe']);
             $("#totaliva").css('font-size','5');
             
           $("#totaliva").append(resultado.cuotasiva[i]['tipo']+" %: "+ resultado.cuotasiva[i]['importe'] + " | ");
          //console.log(resultado.cuotasiva[i]['importe']);
        };
     
       // alert(resultado.cuotasiva[0]['importe']);
       
         //$("#totaliva").val(resultado.cuotasiva.importe);
         //var total_factura  = JSON.stringify(resultado.total + resultado.cuotasiva[0]['importe'] + resultado.cuotasiva[1]['importe'] + resultado.cuotasiva[2]['importe']);
        $("#totaldetalle").val(resultado.total);
//        $("#totalfactura").html(total_factura);
        
         buscoDatosCli();
     
    });
    
   
  
    $.post('procesando.php', {buscar_factura_detalle:albaran_factura,tipo:tipo}, function(resultado_det){
       $("#div_datos_detalle").html(resultado_det); 
    });
}    
 

 
function facturar(){
   var albaran = $("#factura_n").val();
    
   $.post('procesando.php', {generarfactura: albaran}, function(resultado) {  
       alert(resultado);
   });   
      
} 

function pdf_factura(){
  var factura = $("#factura_n").val();
  //location.href="facturaPdf.php?factura="+ factura;
  window.open('facturaPdf.php?factura='+factura,'_blank', 'noopener');
    
}
function cargar_contabilidad(){
   
    
    $("#cargador_modulos").load("contabilidad.php"); 
    
}
function cargar_facturacion(){
   
    
    $("#cargador_modulos").load("facturacion.php"); 
    
}



function mostrar_asientos_maestro(){
   
    var asiento = $("#asiento_filtro").val(); 
    var cliente = $("#cliente_filtro").val(); 
    var fechaini = $("#fecha_ini_filtro").val(); 
    var fechafin = $("#fecha_fin_filtro").val(); 

    $.post('procesando.php', {muestro_maestro_asientos: asiento,cliente:cliente,fechaini:fechaini,fechafin:fechafin}, function(resultado) {  
           if(!resultado){
               alert("El filtro no devolvió ningun dato");
           }else{
            $("#div_datos_maestro_asiento").html(resultado); 
           }
    });        
}
function mostrar_asientos_detalle(nasiento){
   
    var asiento = nasiento;

    $.post('procesando.php', {muestro_detalle_asientos: asiento}, function(resultado) {  
           if(!resultado){
               alert("El filtro no devolvió ningun dato");
           }else{
            $("#div_datos_detalle_asiento").html(resultado); 
           }
    });        
}

function intro_asiento(){
    var subcuenta = $("#subc_asiento").val(); 
    var titulo = $("#titulo_asiento").val();
    var concepto = $("#concepto_asiento").val();
    var debe = $("#debe_asiento").val();
    var haber = $("#haber_asiento").val(); 
    $.post('procesando.php', {intro_asiento: subcuenta,titulo: titulo,concepto: concepto,
                              debe: debe,haber: haber}, function(resultado) {  
           if(!resultado){
               alert("Asiento no insertado.");
           }else{
               alert(resultado);
             //recargamos el detalle del asiento  
               
           }
    });        
    
    
    
    
    
}

//   function llenando_productos(){   
//     
//    $.post('procesando.php', {llenando_select_prod: 1}, function(resultado){
//    console.log(resultado);
//        var items = [
//          resultado  
//        ];
//        
//        
//          $('#nuevo_producto').autocomplete({
//                source:items
//            });   
//      
//    }); 
//       
//       
//   } 
    
//  function selec_option($valor){
//      
//      /* Para obtener el valor */
//        var cod = document.getElementById("nuevo_producto").value;
//        alert(cod);
//
//        /* Para obtener el texto */
//        var combo = document.getElementById("nuevo_producto");
//        var selected = combo.options[combo.selectedIndex].text;
//        alert(selected);
//        $("#nuevo_producto option:selected").text('selected');
////   $("#nuevo_producto").html(selected);
//  }


