<?php


require 'Controllers/conexion.php';
require 'Controllers/clientesController.php';
Require 'Controllers/productosController.php';
Require 'Controllers/facturasController.php';
Require 'Controllers/asientosController.php';

if (isset($_POST['cliente_grabar'])){
    $factura =  $_POST['factura_grabar'];
    $id_cliente = $_POST['cliente_grabar'];
    $fecha_factura = $_POST['fecha_grabar'];
    
    
    $data = new facturaMaestro();
    $facturado = $data->comprobarFacturado($factura);
   
   // if($facturado = 0 or  $facturado = Null){
        $nueva_factura = $data->guardaCabeceraCliente($id_cliente,$fecha_factura,$factura);   
    //}
  

    //$factura = $data->getFactura()+1;
    //echo $factura;
 

}

if (isset($_POST['nueva_factura'])){
    $n_factura = new facturaMaestro;
    $factura = $n_factura->GenerarFacturaNueva();
    $factura = $n_factura->getFactura();
    echo $factura;
    
    
}
if (isset($_POST['mostrar_datos_cli'])){
    $id_cliente = $_POST['mostrar_datos_cli'];
    $data = new clientes;
    $datos_cliente_cabecera = $data->getDatosCliente($id_cliente);
    $nombre = $data->getNombre();
    $rs = $data->getRs();
    $cif = $data->getCif();
    $fp = $data->getFp();
    
    echo ("<div style='font-size:25px;text-align:left'>Nombre : ". $nombre."<br>"."Razon Social : ". $rs. "<br>"."C.I.F : ". $cif. "<br>"."FP : ".$fp."</div>");
    
//    $datos=array("nombre"=>$nombre,
//                "rs"=>$rs,
//                "cif"=>$cif,
//                "fp"=>$fp);
//    echo json_encode($datos);
    
}

if (isset($_POST['elimina_maestro'])){
    echo "Viene eliminar Factura";
    exit;
}
if (isset($_POST['elimina_detalle'])){
    $detalle_eliminar = new facturaDetalle;
    $detalle_eliminar->elimina_detalle($_POST['elimina_detalle']);
    
}

if(isset($_POST['actualizar_detalle'])){
     $nom_producto = $_POST['actualizar_detalle'];
     $cantidad = $_POST['nueva_cantidad'];
     $descuento = $_POST['nuevo_dto']; 
     $id_detalle = $_POST['id_detalle']; 
     $precio = $_POST['nuevo_precio'];
   
     $detalle_factura = new facturaDetalle;

     $actualizandoDetalle = $detalle_factura->guardarDetalleFactura('0', $nom_producto, $cantidad, $descuento,$id_detalle,$precio);
    
}

 if (isset($_POST['grabarProductoDet'])){
     $nom_producto = $_POST['grabarProductoDet'];
     $cantidad = $_POST['grabarCantidadDet'];
     $descuento = $_POST['grabarDescuentoDet']; 
     $id_factura = $_POST['grabarFacturaDet'];
     $precio_actualizar = 0;//pasamos solo el parametro con valor 0
     $id_detalle = 0;//pasamos el id_detalle a 0 pq es un insert y de esta manera podemos reutilizar la funcion tanto para insertar como para actualizar
    //echo "<p>$id_producto.$cantidad.$descuento.$id_factura</p>";     
     
     $detalle_factura = new facturaDetalle;

     $guardandoDetalle = $detalle_factura->guardarDetalleFactura($id_factura, $nom_producto, $cantidad, $descuento,$id_detalle,$precio_actualizar);

//     $mostrando_detalle = new facturaDetalle;
//     $mostrar_todo_detalle = $mostrando_detalle->mostrarDetalleFactura($id_factura);
  
}   

if (isset($_POST['mostrandoDetalle'])){
    $id_factura = $_POST['mostrandoDetalle'];
    $detalle_factura = new facturaDetalle;
    $mostrar_todo_detalle = $detalle_factura->mostrarDetalleFactura($id_factura,'a');
    
}


if (isset($_POST['buscar_factura_maestro'])){
    
    
    header('Content-type: application/json');
 
    $datos_maestro = new facturaMaestro;
    $datos_cab = $datos_maestro->getDatosMaestro($_POST['buscar_factura_maestro'],$_POST['tipo']);
    $factura = $datos_maestro->getId();
    $nfacturacion = $datos_maestro->getfactura();
    $cliente = $datos_maestro->getidCliente();
    $fecha = $datos_maestro->getFecha();
    $total = $datos_maestro->getTotal();
    //$cuotasiva = $datos_maestro->getVaciarArrayCuotas();
    $cuotasiva = $datos_maestro->getCuotaIva();
  
    // vaciamos el array de cuotas para mostrar las nuevas
    //$datos_maestro->getVaciarArrayCuotas();
    
    $array_maestro =array(
        "factura"=>$factura,
        "nfacturacion" => $nfacturacion,
        "cliente"=>$cliente,
        "fecha"=>$fecha,
        "total"=>$total,
        "cuotasiva"=>$cuotasiva 
    );
    
  
    
    
    
    echo json_encode($array_maestro, JSON_FORCE_OBJECT);
} 
    
 if (isset($_POST['buscar_factura_detalle'])){
        $id_factura = $_POST['buscar_factura_detalle'];
        $tipo = $_POST['tipo'];
        $detalle_factura = new facturaDetalle;
        $mostrar_todo_detalle = $detalle_factura->mostrarDetalleFactura($id_factura,$tipo);
     
 }   

 
 if (isset($_POST['generarfactura'])){
     
     // grabar update nuevo numero de factura
     $GrabarNuevaFactura = new facturaMaestro;
     $GrabarNuevaFactura->GrabarFactura($_POST['generarfactura']);
     
    // generando asiento contable
      //obtenemos el id de cliente y el total de la factura para generar el asiento contable
     $cliente = $GrabarNuevaFactura->getDatosMaestro($_POST['generarfactura'],'a');
     $id_cliente = $GrabarNuevaFactura->getidCliente();
     $total_factura = $GrabarNuevaFactura->getTotal();
     $factura = $GrabarNuevaFactura->getfactura();
     $fechaFactura = $GrabarNuevaFactura->getFechaFactura();
     $sumaCuotas = $GrabarNuevaFactura->getSumaCuotasFactura();
      //----------------------------
     $GenerarAsiento = new asientos;
     $GenerarAsiento->GenerarAsientoFactura($factura,$total_factura,$fechaFactura,$id_cliente,$sumaCuotas);
     
     
 }
    
//   if (isset($_POST['llenando_select_prod'])){
//       
//       $select_productos = new productos();
//       $seleccion = $select_productos->getSelectProductos();
//       echo $seleccion;
//       
//       
//   }
//    
    
    

?>
