﻿CREATE TRIGGER INSERTAR_ASIENTO_BI 
 ON factura_maestro 
  BEFORE INSERT 
  AS
  IF EXISTS (SELECT * FROM asiento_contable WHERE nasiento = new.id)
  THEN 
  BEGIN
  DECLARE porcentajeiva int
  DECLARE subcuenta varchar(11)
  subcuenta_ =  CONCAT( '700.0.',RIGHT(REPLICATE('0', 6)+ CAST(NEW.id AS VARCHAR(6)), 6))
  SELECT  
    UPDATE asiento_contable
    set subcuenta = subcuenta_ WHERE nasiento = new.id AND titulo = 'Venta de productos'
END
ELSE
BEGIN
INSERT INTO asiento_contable VALUES('subcuenta_','Ventas de productos','Venta de productos','new.subtotal')

END
GO

