-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 12-09-2019 a las 00:05:17
-- Versión del servidor: 10.1.32-MariaDB
-- Versión de PHP: 7.0.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `facturacion`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asiento_contable_det`
--

CREATE TABLE `asiento_contable_det` (
  `id` int(11) NOT NULL,
  `nasiento` int(10) DEFAULT NULL,
  `subcuenta` varchar(15) DEFAULT NULL,
  `titulo` varchar(200) DEFAULT NULL,
  `concepto` varchar(200) DEFAULT NULL,
  `debe` float DEFAULT NULL,
  `haber` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `asiento_contable_det`
--

INSERT INTO `asiento_contable_det` (`id`, `nasiento`, `subcuenta`, `titulo`, `concepto`, `debe`, `haber`) VALUES
(12, 59, '700.0.00001', 'Ventas de productos', 'Venta de mercaderias', 0, 1040.32),
(13, 59, '430.0.00002', 'Clientes, Público en general', 'Venta de mercaderias', 1099.99, 0),
(14, 59, '477.0.00021', 'HP IVA repercutido 21%', 'Venta de productos', 0, 5.292),
(15, 59, '477.0.00010', 'HP IVA repercutido 10%', 'Venta de productos', 0, 22.95),
(16, 59, '477.0.00004', 'HP IVA repercutido 4%', 'Venta de productos', 0, 31.4248),
(23, 60, '700.0.00001', 'Ventas de productos', 'Venta de mercaderias', 0, 726.08),
(24, 60, '430.0.00002', 'Clientes, Público en general', 'Venta de mercaderias', 773.177, 0),
(25, 60, '477.0.00021', 'HP IVA repercutido 21%', 'Venta de productos', 0, 5.292),
(26, 60, '477.0.00010', 'HP IVA repercutido 10%', 'Venta de productos', 0, 22.95),
(27, 60, '477.0.00004', 'HP IVA repercutido 4%', 'Venta de productos', 0, 18.8552),
(28, 61, '700.0.00001', 'Ventas de productos', 'Venta de mercaderias', 0, 656.46),
(29, 61, '430.0.00002', 'Clientes, Público en general', 'Venta de mercaderias', 775.912, 0),
(30, 61, '477.0.00021', 'HP IVA repercutido 21%', 'Venta de productos', 0, 111.72),
(31, 61, '477.0.00010', 'HP IVA repercutido 10%', 'Venta de productos', 0, 4.59),
(32, 61, '477.0.00004', 'HP IVA repercutido 4%', 'Venta de productos', 0, 3.1424);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asiento_contable_enca`
--

CREATE TABLE `asiento_contable_enca` (
  `id` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `asiento` int(11) NOT NULL,
  `titulo` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `asiento_contable_enca`
--

INSERT INTO `asiento_contable_enca` (`id`, `fecha`, `asiento`, `titulo`) VALUES
(61, '2019-08-16', 43, 'Venta de Productos'),
(62, '2019-08-16', 44, 'Venta de Productos'),
(63, '2019-08-16', 45, 'Venta de Productos'),
(64, '2019-08-16', 45, 'Venta de Productos'),
(65, '2019-08-16', 46, 'Venta de Productos'),
(66, '2019-08-16', 47, 'Venta de Productos'),
(67, '2019-08-16', 48, 'Venta de Productos'),
(68, '2019-08-16', 49, 'Venta de Productos'),
(69, '2019-08-16', 50, 'Venta de Productos'),
(70, '2019-08-16', 51, 'Venta de Productos'),
(71, '2019-08-16', 52, 'Venta de Productos'),
(72, '2019-08-16', 53, 'Venta de Productos'),
(73, '2019-08-16', 54, 'Venta de Productos'),
(74, '2019-08-16', 55, 'Venta de Productos'),
(75, '2019-08-16', 56, 'Venta de Productos'),
(80, '2019-08-16', 57, 'Venta de Productos'),
(81, '2019-08-16', 58, 'Venta de Productos'),
(82, '2019-08-16', 59, 'Venta de Productos'),
(85, '2019-08-16', 60, 'Venta de Productos'),
(86, '2019-08-25', 61, 'Venta de Productos');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE `clientes` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `rs` varchar(50) NOT NULL,
  `cif` varchar(11) NOT NULL,
  `fp` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`id`, `nombre`, `rs`, `cif`, `fp`) VALUES
(1, 'Suministros Pepito,S.L.', 'Pepito Perez', 'A79290326', 'contado'),
(2, 'Aluminios Maripili, S.A.', 'Maripili Fernandez', 'A48324651', 'transferencia');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuotasiva`
--

CREATE TABLE `cuotasiva` (
  `id` int(11) NOT NULL,
  `tipo` int(11) NOT NULL,
  `importe` double(8,4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cuotasiva`
--

INSERT INTO `cuotasiva` (`id`, `tipo`, `importe`) VALUES
(6, 2, 50.4900),
(8, 1, 50.2740),
(9, 1, 27.9300),
(10, 1, 3.5280),
(11, 1, 27.9300),
(17, 2, 13.7700),
(18, 2, 45.6960),
(19, 1, 60.9630),
(20, 2, 4.5900),
(21, 1, 27.9300),
(21, 2, 24.2250),
(21, 3, 19.9024),
(22, 2, 4.8450),
(22, 3, 39.8048),
(23, 1, 61.4460),
(23, 2, 5.1000),
(23, 3, 47.7660),
(24, 1, 62.4456),
(25, 1, 27.9300),
(26, 3, 4.1900),
(27, 1, 5.5860),
(28, 2, 24.4800),
(29, 2, 48.4500),
(30, 1, 1.1760),
(31, 1, 5.8212),
(31, 3, 4.1480),
(32, 1, 5.8212),
(33, 2, 65.8920),
(34, 2, 43.6050),
(35, 3, 3.9804),
(36, 1, 50.2740),
(36, 3, 42.9892),
(37, 1, 5.5860),
(38, 1, 5.8800),
(38, 2, 5.1000),
(38, 3, 4.1900),
(39, 1, 5.8800),
(39, 2, 5.1000),
(39, 3, 4.1900),
(40, 1, 5.8800),
(40, 2, 5.1000),
(40, 3, 4.1900),
(41, 1, 5.8800),
(41, 2, 5.1000),
(42, 1, 5.8800),
(42, 2, 5.1000),
(42, 3, 4.1900),
(43, 1, 5.8800),
(43, 2, 5.1000),
(43, 3, 4.1900),
(44, 1, 5.8800),
(44, 2, 5.1000),
(44, 3, 4.1900),
(45, 1, 5.8800),
(45, 2, 5.1000),
(45, 3, 4.1900),
(46, 1, 5.8800),
(47, 1, 5.8800),
(48, 1, 5.8800),
(48, 2, 5.1000),
(48, 3, 4.1900),
(49, 1, 5.8800),
(50, 2, 5.1000),
(51, 1, 5.8800),
(52, 1, 5.8800),
(53, 3, 4.1900),
(54, 2, 5.1000),
(55, 3, 4.1900),
(56, 3, 4.1900),
(58, 1, 1.1760),
(58, 2, 24.2250),
(58, 3, 41.4812),
(59, 1, 5.2920),
(59, 2, 22.9500),
(59, 3, 31.4248),
(60, 1, 5.2920),
(60, 2, 22.9500),
(60, 3, 18.8552),
(61, 1, 111.7200),
(61, 2, 4.5900),
(61, 3, 3.1424);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `factura_detalle`
--

CREATE TABLE `factura_detalle` (
  `id` int(11) NOT NULL,
  `id_factura` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precio` float NOT NULL,
  `descuento` int(11) NOT NULL,
  `subtotal` double(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `factura_detalle`
--

INSERT INTO `factura_detalle` (`id`, `id_factura`, `id_producto`, `cantidad`, `precio`, `descuento`, `subtotal`) VALUES
(20, 7, 1, 200, 10, 10, 20.00),
(21, 7, 1, 200, 10, 10, 20.00),
(27, 7, 1, 100, 5.6, 10, 56.00),
(32, 7, 1, 200, 5.6, 5, 1064.00),
(35, 7, 1, 1, 5.6, 10, 5.04),
(39, 7, 1, 200, 5.6, 5, 1064.00),
(42, 12, 1, 200, 5.6, 10, 1008.00),
(43, 13, 1, 105, 5.6, 10, 529.20),
(46, 14, 1, 200, 5.6, 5, 1064.00),
(47, 22, 1, 200, 5.6, 10, 1008.00),
(48, 22, 1, 105, 5.6, 5, 558.60),
(49, 22, 1, 20, 5.6, 10, 100.80),
(56, 22, 1, 200, 5.6, 10, 1008.00),
(74, 24, 1, 200, 5.6, 10, 1008.00),
(86, 25, 2, 23, 10.2, 10, 211.14),
(87, 25, 1, 1, 5.6, 12, 4.93),
(100, 21, 1, 5, 5.6, 0, 28.00),
(101, 21, 2, 5, 10.2, 0, 51.00),
(102, 21, 1, 5, 25, 0, 125.00),
(103, 17, 2, 15, 10.2, 10, 137.70),
(104, 6, 2, 55, 10.2, 10, 504.90),
(105, 8, 1, 45, 5.6, 5, 239.40),
(106, 9, 1, 25, 5.6, 5, 133.00),
(107, 10, 1, 4, 5.6, 25, 16.80),
(108, 11, 1, 25, 5.6, 5, 133.00),
(109, 18, 2, 56, 10.2, 20, 456.96),
(110, 23, 2, 5, 10.2, 0, 51.00),
(111, 26, 1, 5, 5.6, 0, 28.00),
(112, 28, 2, 34, 10.2, 5, 329.46),
(113, 19, 1, 54, 5.6, 4, 290.30),
(114, 20, 2, 5, 10.2, 10, 45.90),
(115, 27, 1, 35, 5.6, 5, 186.20),
(118, 29, 1, 25, 5.6, 5, 133.00),
(119, 29, 2, 25, 10.2, 5, 242.25),
(120, 29, 3, 25, 20.95, 5, 497.56),
(121, 30, 2, 5, 10.2, 5, 48.45),
(122, 30, 3, 50, 20.95, 5, 995.12),
(123, 31, 1, 55, 5.6, 5, 292.60),
(124, 31, 3, 60, 20.95, 5, 1194.15),
(125, 32, 1, 59, 5.6, 10, 297.36),
(126, 33, 1, 25, 5.6, 5, 133.00),
(127, 34, 3, 5, 20.95, 0, 104.75),
(128, 35, 1, 5, 5.6, 5, 26.60),
(129, 36, 3, 54, 20.95, 5, 1074.73),
(130, 36, 1, 45, 5.6, 5, 239.40),
(131, 37, 2, 25, 10.2, 4, 244.80),
(132, 38, 2, 50, 10.2, 5, 484.50),
(133, 39, 1, 1, 5.6, 0, 5.60),
(134, 40, 1, 5, 5.6, 1, 27.72),
(135, 40, 3, 5, 20.95, 1, 103.70),
(136, 41, 1, 5, 5.6, 1, 27.72),
(137, 42, 2, 68, 10.2, 5, 658.92),
(138, 43, 2, 45, 10.2, 5, 436.05),
(139, 44, 3, 5, 20.95, 5, 99.51),
(141, 45, 2, 25, 10.2, 5, 242.25),
(142, 45, 3, 25, 20.95, 5, 497.56),
(143, 46, 1, 5, 5.6, 5, 26.60),
(144, 47, 1, 5, 5.6, 0, 28.00),
(145, 47, 2, 5, 10.2, 0, 51.00),
(146, 47, 3, 5, 20.95, 0, 104.75),
(147, 48, 1, 5, 5.6, 0, 28.00),
(148, 48, 2, 5, 10.2, 0, 51.00),
(149, 48, 3, 5, 20.95, 0, 104.75),
(150, 49, 1, 5, 5.6, 0, 28.00),
(151, 49, 2, 5, 10.2, 0, 51.00),
(152, 49, 3, 5, 20.95, 0, 104.75),
(153, 50, 1, 5, 5.6, 0, 28.00),
(154, 50, 2, 5, 10.2, 0, 51.00),
(155, 51, 1, 5, 5.6, 0, 28.00),
(156, 51, 2, 5, 10.2, 0, 51.00),
(157, 51, 3, 5, 20.95, 0, 104.75),
(159, 52, 1, 5, 5.6, 0, 28.00),
(160, 52, 2, 5, 10.2, 0, 51.00),
(161, 52, 3, 5, 20.95, 0, 104.75),
(162, 53, 1, 5, 5.6, 0, 28.00),
(163, 53, 2, 5, 10.2, 0, 51.00),
(164, 53, 3, 5, 20.95, 0, 104.75),
(165, 54, 1, 5, 5.6, 0, 28.00),
(166, 54, 2, 5, 10.2, 0, 51.00),
(167, 54, 3, 5, 20.95, 0, 104.75),
(168, 55, 1, 5, 5.6, 0, 28.00),
(169, 56, 1, 5, 5.6, 0, 28.00),
(170, 57, 1, 5, 5.6, 0, 28.00),
(171, 57, 2, 5, 10.2, 0, 51.00),
(172, 57, 3, 5, 20.95, 0, 104.75),
(173, 58, 1, 5, 5.6, 0, 28.00),
(174, 59, 2, 5, 10.2, 0, 51.00),
(175, 60, 1, 5, 5.6, 0, 28.00),
(176, 61, 1, 5, 5.6, 0, 28.00),
(177, 62, 3, 5, 20.95, 0, 104.75),
(179, 63, 2, 5, 10.2, 0, 51.00),
(180, 64, 3, 5, 20.95, 0, 104.75),
(181, 65, 3, 5, 20.95, 0, 104.75),
(182, 66, 3, 1, 20.95, 0, 20.95),
(183, 67, 1, 1, 5.6, 0, 5.60),
(184, 67, 2, 25, 10.2, 5, 242.25),
(185, 67, 3, 55, 20.95, 10, 1037.03),
(186, 68, 1, 5, 5.6, 10, 25.20),
(187, 68, 2, 25, 10.2, 10, 229.50),
(188, 68, 3, 50, 20.95, 25, 785.62),
(194, 69, 1, 5, 5.6, 10, 25.20),
(195, 69, 2, 25, 10.2, 10, 229.50),
(196, 69, 3, 25, 20.95, 10, 471.38),
(197, 70, 3, 5, 20.95, 25, 78.56),
(198, 70, 2, 5, 10.2, 10, 45.90),
(199, 70, 1, 100, 5.6, 5, 532.00);

--
-- Disparadores `factura_detalle`
--
DELIMITER $$
CREATE TRIGGER `ACTUALIZAR_TOTALES_BU` BEFORE UPDATE ON `factura_detalle` FOR EACH ROW UPDATE factura_maestro SET
total = (total - old.subtotal)+(new.subtotal)
where id = new.id_factura
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `ACTUALIZA_CUOTAS_IVA_AD` AFTER DELETE ON `factura_detalle` FOR EACH ROW BEGIN
DECLARE antiguoiva float DEFAULT 0;  -- En esta variable voy calculando la nueva cuota de iva para insertar o actualizar en la tabla cuotasiva.
DECLARE ivaprod int;  -- En esta variable quiero almacenar el tipo de iva del producto insertado. Al guardar un producto nuevo comparo su id con el de productos y saco el tipo de iva(ivaprod) del mismo.
DECLARE porceniva int; -- Con el tipo de iva consulto en la tabla tiposiva y obtengo el porcentaje para hacer los calculos sobre la nueva cuota.

 SELECT productos.iva,tiposiva.porc INTO ivaprod,porceniva from productos JOIN tiposiva on productos.iva = tiposiva.id where productos.id = old.id_producto;

SET antiguoiva = old.subtotal * (porceniva/100);

IF EXISTS (SELECT id,tipo FROM cuotasiva WHERE id=old.id_factura and tipo = ivaprod) THEN
    -- Despues de eliminar el registro de detalle actualizamos nuevamente la cuota de iva restandole el iva correspondiente al detalle eliminado.
     UPDATE cuotasiva SET importe = importe - antiguoiva 
    WHERE id = old.id_factura and tipo = ivaprod;
END IF;
IF EXISTS (SELECT id,tipo FROM cuotasiva WHERE id=old.id_factura and tipo = ivaprod AND importe = 0) THEN
    DELETE FROM cuotasiva WHERE ID = OLD.id_factura AND tipo = ivaprod;
END IF;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `ACTUALIZA_CUOTAS_IVA_AI_O_AU` AFTER INSERT ON `factura_detalle` FOR EACH ROW BEGIN
DECLARE nuevoiva float DEFAULT 0;  -- En esta variable voy calculando la nueva cuota de iva para insertar o actualizar en la tabla cuotasiva.
DECLARE ivaprod int;  -- En esta variable quiero almacenar el tipo de iva del producto insertado. Al guardar un producto nuevo comparo su id con el de productos y saco el tipo de iva(ivaprod) del mismo.
DECLARE porceniva int; -- Con el tipo de iva consulto en la tabla tiposiva y obtengo el porcentaje para hacer los calculos sobre la nueva cuota.

 SELECT productos.iva,tiposiva.porc INTO ivaprod,porceniva from productos JOIN tiposiva on productos.iva = tiposiva.id where productos.id = new.id_producto;
-- SELECT iva ivaprod from productos where id = new.id_producto
-- SELECT porc porceniva from tiposiva where id = ivaprod

SET nuevoiva = new.subtotal * (porceniva/100);  -- aqui calculo la nueva cuota de iva del detalle insertado

IF EXISTS (SELECT id,tipo FROM cuotasiva WHERE id=new.id_factura and tipo = ivaprod) THEN
    UPDATE cuotasiva SET importe = importe + nuevoiva
    WHERE id = new.id_factura and tipo = ivaprod;
ELSE
    INSERT INTO cuotasiva VALUES (new.id_factura,ivaprod,nuevoiva);
END IF;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `ACTUALIZA_CUOTAS_IVA_AU` AFTER UPDATE ON `factura_detalle` FOR EACH ROW BEGIN
DECLARE antiguoiva float DEFAULT 0;  -- En esta variable voy calculando la nueva cuota de iva para insertar o actualizar en la tabla cuotasiva.
DECLARE nuevoiva float DEFAULT 0;
DECLARE ivaprod int;  -- En esta variable quiero almacenar el tipo de iva del producto insertado. Al guardar un producto nuevo comparo su id con el de productos y saco el tipo de iva(ivaprod) del mismo.
DECLARE porceniva int; -- Con el tipo de iva consulto en la tabla tiposiva y obtengo el porcentaje para hacer los calculos sobre la nueva cuota.

 SELECT productos.iva,tiposiva.porc INTO ivaprod,porceniva from productos JOIN tiposiva on productos.iva = tiposiva.id where productos.id = old.id_producto;

SET antiguoiva = old.subtotal * (porceniva/100);
SET nuevoiva = new.subtotal * (porceniva/100);

IF EXISTS (SELECT id,tipo FROM cuotasiva WHERE id=old.id_factura and tipo = ivaprod) THEN
   -- actualizamos la cuota actual descontandole la modificacion
    UPDATE cuotasiva SET importe = importe - antiguoiva 
    WHERE id = old.id_factura and tipo = ivaprod;
    -- actualizamos la cuota con el nuevo iva de producto
     UPDATE cuotasiva SET importe = importe + nuevoiva 
    WHERE id = new.id_factura and tipo = ivaprod;
END IF;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `ACTUALIZA_TOTALES_BD` BEFORE DELETE ON `factura_detalle` FOR EACH ROW UPDATE factura_maestro SET total = (TOTAL - OLD.subtotal)where id = old.id_factura
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `ACTUALIZA_TOTALES_BI` BEFORE INSERT ON `factura_detalle` FOR EACH ROW UPDATE factura_maestro SET total = (TOTAL + NEW.subtotal)where id = NEW.id_factura
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `factura_maestro`
--

CREATE TABLE `factura_maestro` (
  `id` int(11) NOT NULL,
  `id_cte` int(11) NOT NULL,
  `factura` int(11) DEFAULT NULL,
  `cuotaiva` double(8,2) NOT NULL,
  `total` double(10,2) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `fecha_factura` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `factura_maestro`
--

INSERT INTO `factura_maestro` (`id`, `id_cte`, `factura`, `cuotaiva`, `total`, `fecha`, `fecha_factura`) VALUES
(1, 2, NULL, 0.00, 0.00, '2019-07-16', '0000-00-00'),
(2, 2, NULL, 0.00, 0.00, '2019-07-16', '0000-00-00'),
(3, 1, NULL, 0.00, 0.00, '2019-07-31', '0000-00-00'),
(4, 2, NULL, 0.00, 0.00, '2019-07-10', '0000-00-00'),
(5, 2, NULL, 0.00, 0.00, '2019-07-30', '0000-00-00'),
(6, 2, 6, 0.00, 504.90, '2019-08-11', '0000-00-00'),
(7, 1, 4, 0.00, 0.00, '2019-08-11', '0000-00-00'),
(8, 2, 7, 0.00, 239.40, '2019-08-11', '0000-00-00'),
(9, 1, 8, 0.00, 133.00, '2019-08-11', '0000-00-00'),
(10, 2, 9, 0.00, 16.80, '2019-08-11', '0000-00-00'),
(11, 2, 10, 0.00, 133.00, '2019-08-11', '0000-00-00'),
(12, 2, 5, 0.00, 0.00, '2019-08-11', '0000-00-00'),
(13, 1, 3, 0.00, 0.00, '2019-08-11', '0000-00-00'),
(14, 1, NULL, 0.00, 0.00, '2019-07-19', '0000-00-00'),
(15, 2, NULL, 0.00, 0.00, '2019-07-19', '0000-00-00'),
(16, 2, NULL, 0.00, 0.00, '2019-07-19', '0000-00-00'),
(17, 1, 2, 0.00, 16105.09, '2019-08-11', '0000-00-00'),
(18, 1, 11, 0.00, 456.96, '2019-08-11', '0000-00-00'),
(19, 1, 17, 0.00, 290.30, '2019-07-30', '2019-08-13'),
(20, 1, 18, 0.00, 45.90, '2019-08-13', '2019-08-13'),
(21, 2, 12, 0.00, 204.00, '2019-08-11', '0000-00-00'),
(22, 1, 1, 0.00, 15462.80, '2019-07-30', '0000-00-00'),
(23, 2, 13, 0.00, 51.00, '2019-08-11', '0000-00-00'),
(24, 2, 20, 0.00, 16875.40, '2019-07-24', '2019-08-14'),
(25, 2, 14, 0.00, 216.07, '2019-08-11', '0000-00-00'),
(26, 2, 15, 0.00, 28.00, '2019-08-11', '0000-00-00'),
(27, 2, 19, 0.00, 186.20, '0000-00-00', '2019-08-14'),
(28, 2, 16, 0.00, 329.46, '2019-08-11', '0000-00-00'),
(29, 2, 21, 0.00, 872.81, '2019-08-14', '2019-08-15'),
(30, 2, 22, 0.00, 1043.57, '2019-08-15', '2019-08-15'),
(31, 2, 23, 0.00, 1486.75, '2019-08-15', '2019-08-15'),
(32, 1, 24, 0.00, 297.36, '2019-08-15', '2019-08-15'),
(33, 1, 25, 0.00, 133.00, '2019-08-15', '2019-08-15'),
(34, 2, 26, 0.00, 104.75, '2019-08-15', '2019-08-15'),
(35, 2, 27, 0.00, 26.60, '2019-08-15', '2019-08-15'),
(36, 2, 0, 0.00, 1314.13, '2019-08-15', '0000-00-00'),
(37, 2, 28, 0.00, 244.80, '2019-08-15', '2019-08-15'),
(38, 2, 29, 0.00, 484.50, '2019-08-15', '2019-08-15'),
(39, 1, 30, 0.00, 5.60, '2019-08-15', '2019-08-15'),
(40, 2, 31, 0.00, 131.42, '2019-08-15', '2019-08-15'),
(41, 2, 32, 0.00, 27.72, '2019-08-15', '2019-08-15'),
(42, 2, 33, 0.00, 658.92, '2019-08-15', '2019-08-15'),
(43, 1, 34, 0.00, 436.05, '2019-08-15', '2019-08-15'),
(44, 1, 35, 0.00, 99.51, '2019-08-15', '2019-08-15'),
(45, 1, 36, 0.00, 739.81, '2019-08-16', '2019-08-16'),
(46, 2, 37, 0.00, 26.60, '2019-08-16', '2019-08-16'),
(47, 2, 38, 0.00, 183.75, '2019-08-16', '2019-08-16'),
(48, 2, 39, 0.00, 183.75, '2019-08-16', '2019-08-16'),
(49, 1, 40, 0.00, 183.75, '2019-08-16', '2019-08-16'),
(50, 2, 41, 0.00, 79.00, '2019-08-16', '2019-08-16'),
(51, 1, 42, 0.00, 183.75, '2019-08-16', '2019-08-16'),
(52, 2, 43, 0.00, 183.75, '2019-08-16', '2019-08-16'),
(53, 2, 44, 0.00, 183.75, '2019-08-16', '2019-08-16'),
(54, 1, 45, 0.00, 183.75, '2019-08-16', '2019-08-16'),
(55, 1, 46, 0.00, 28.00, '2019-08-16', '2019-08-16'),
(56, 2, 47, 0.00, 28.00, '2019-08-16', '2019-08-16'),
(57, 2, 48, 0.00, 183.75, '2019-08-16', '2019-08-16'),
(58, 1, 49, 0.00, 28.00, '2019-08-16', '2019-08-16'),
(59, 2, 50, 0.00, 51.00, '2019-08-16', '2019-08-16'),
(60, 1, 51, 0.00, 28.00, '2019-08-16', '2019-08-16'),
(61, 2, 52, 0.00, 28.00, '2019-08-16', '2019-08-16'),
(62, 1, 53, 0.00, 104.75, '2019-08-16', '2019-08-16'),
(63, 1, 54, 0.00, 51.00, '2019-08-16', '2019-08-16'),
(64, 2, 55, 0.00, 104.75, '2019-08-16', '2019-08-16'),
(65, 1, 56, 0.00, 104.75, '2019-08-16', '2019-08-16'),
(66, 2, 57, 0.00, 20.95, '2019-08-16', '2019-08-16'),
(67, 2, 58, 0.00, 1284.88, '2019-08-16', '2019-08-16'),
(68, 2, 59, 0.00, 1040.32, '2019-08-16', '2019-08-16'),
(69, 2, 60, 0.00, 726.08, '2019-08-16', '2019-08-16'),
(70, 2, 61, 0.00, 656.46, '2019-08-22', '2019-08-25');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `descripcion` varchar(200) NOT NULL,
  `iva` int(2) NOT NULL,
  `tarifa` decimal(4,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`id`, `nombre`, `descripcion`, `iva`, `tarifa`) VALUES
(1, 'baralu200', 'Barra de aluminio de 200ml.', 1, '5.60'),
(2, 'escu100', 'Escuadra de aluminio de 100ml', 2, '10.20'),
(3, 'regla3m', 'regla de 3 metros', 3, '20.95');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tiposiva`
--

CREATE TABLE `tiposiva` (
  `id` int(2) NOT NULL,
  `porc` int(11) DEFAULT '21',
  `descripcion` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tiposiva`
--

INSERT INTO `tiposiva` (`id`, `porc`, `descripcion`) VALUES
(1, 21, 'General'),
(2, 10, 'Reducido'),
(3, 4, 'Superreducido');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `asiento_contable_det`
--
ALTER TABLE `asiento_contable_det`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_asientosFacturaMaestro` (`nasiento`) USING BTREE;

--
-- Indices de la tabla `asiento_contable_enca`
--
ALTER TABLE `asiento_contable_enca`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cuotasiva`
--
ALTER TABLE `cuotasiva`
  ADD PRIMARY KEY (`id`,`tipo`),
  ADD UNIQUE KEY `id` (`id`,`tipo`);

--
-- Indices de la tabla `factura_detalle`
--
ALTER TABLE `factura_detalle`
  ADD PRIMARY KEY (`id`,`id_factura`,`id_producto`),
  ADD UNIQUE KEY `id` (`id`,`id_factura`,`id_producto`),
  ADD KEY `FK_maestroDetalle` (`id_factura`),
  ADD KEY `FK_DetalleProductos` (`id_producto`);

--
-- Indices de la tabla `factura_maestro`
--
ALTER TABLE `factura_maestro`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `FK_facturaclientes` (`id_cte`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_productos_tiposiva_id` (`iva`);

--
-- Indices de la tabla `tiposiva`
--
ALTER TABLE `tiposiva`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `asiento_contable_det`
--
ALTER TABLE `asiento_contable_det`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT de la tabla `asiento_contable_enca`
--
ALTER TABLE `asiento_contable_enca`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=87;

--
-- AUTO_INCREMENT de la tabla `clientes`
--
ALTER TABLE `clientes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `factura_detalle`
--
ALTER TABLE `factura_detalle`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=200;

--
-- AUTO_INCREMENT de la tabla `factura_maestro`
--
ALTER TABLE `factura_maestro`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `factura_detalle`
--
ALTER TABLE `factura_detalle`
  ADD CONSTRAINT `FK_DetalleProductos` FOREIGN KEY (`id_producto`) REFERENCES `productos` (`id`),
  ADD CONSTRAINT `FK_maestroDetalle` FOREIGN KEY (`id_factura`) REFERENCES `factura_maestro` (`id`);

--
-- Filtros para la tabla `factura_maestro`
--
ALTER TABLE `factura_maestro`
  ADD CONSTRAINT `FK_facturaclientes` FOREIGN KEY (`id_cte`) REFERENCES `clientes` (`id`);

--
-- Filtros para la tabla `productos`
--
ALTER TABLE `productos`
  ADD CONSTRAINT `FK_productos_tiposiva_id` FOREIGN KEY (`iva`) REFERENCES `tiposiva` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
