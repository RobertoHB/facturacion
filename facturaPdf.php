<?php
//cargamos la libreria que contiene la libreria mpdf
require_once('librerias/vendor/autoload.php');
//Cargamos una plantilla ya configurada previamente en html
require_once('plantillas/informe/plantilla.php');
//Cargamos el codigo css de la plantilla
$css = file_get_contents('css/plantilla_pdf.css');

//recogemos por get la factura enviada desde el formulario por js
if (isset($_GET['factura'])){
    $factura = $_GET['factura'];
}else{
exit;    
}
//Base de datos




//Creamos un nuevo objeto mpdf

$mpdf = new \Mpdf\Mpdf([
    
]);

$plantilla = getPlantillaPdf($factura);
//mediante una funcion cargamos la plantilla html en la variable $plantilla
//$plantilla = getPlantilla();


//Comenzamos a crear el pdf cargando los css y el html

$mpdf->WriteHTML($css, \Mpdf\HTMLParserMode::HEADER_CSS);
$mpdf->WriteHTML($plantilla, \Mpdf\HTMLParserMode::HTML_BODY);

$mpdf->Output();



?>
