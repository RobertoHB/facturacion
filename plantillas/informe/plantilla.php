<?php
Require 'Controllers/conexion.php';
Require 'Controllers/clientesController.php';
Require 'Controllers/productosController.php';
Require 'Controllers/facturasController.php';

function getPlantillaPdf($factura){
  
//obtenemos los datos de cabecera de la factura

$cabecera_factura = new facturaMaestro;
$cabecera_factura->getDatosMaestro($factura, 'a');
$fecha = $cabecera_factura->getFechaFactura();
$factura = $cabecera_factura->getfactura();
$cliente = $cabecera_factura->getidCliente();
$total_detalle = $cabecera_factura->getTotal();
$total_iva = $cabecera_factura->getCuotaIva();

$total_cuotas = $cabecera_factura->getSumaCuotasFactura();
$total_factura = $total_detalle+$total_cuotas;
        
        
$data = new clientes;
$datos_cliente_cabecera = $data->getDatosCliente($cliente);
$nombreCliente = $data->getNombre();
$rsCliente = $data->getRs();
$cifCliente = $data->getCif();
$fpCliente = $data->getFp();

$detalle_factura = new facturaDetalle;
$detalle_plantilla = $detalle_factura->mostrarDetallepdf($factura);


//var_dump($detalle_plantilla);
//die();

$plantilla ="<div class='row'>
                <div class='cabecera1 datos_factura'>
                   <h2>Factura</h2>
                   <h4>Fecha.  $fecha</h4>
                   <h4>Factura Nº.  $factura</h4>
                </div>
                
                <div class='cabecera1 datos_cliente'>
                  <h2>Facturar a</h2>
                  <h4>Nombre. $nombreCliente</p>
                  <h4>Razón Social.  $rsCliente</h4>
                  <h4>C.I.F.   $cifCliente</h4>
                 <h4>F.P.   $fpCliente</h4>
                </div>
            </div>";
    $plantilla .= "<table class='table3'><thead>
           <tr><th colspan='1'>Articulo</th><th colspan='1'>Descripcion</th><th colspan='1'>Cantidad</th><th colspan='1'>Precio</th><th colspan='1'>Descuento</th><th colspan='2'>Subtotal</th></tr></thead><tbody>";
  
    foreach ($detalle_plantilla as $row) {
     $plantilla .= "<tr>
               <td colspan='1' style='text-align:center;'>".$row['producto']."</td>
               <td colspan='1' style='text-align:center;'>".$row['descripcion']."</td>
               <td colspan='1'  style='text-align:center;'>".$row['cantidad']."</td>
               <td colspan='1'  style='text-align:center;'>".$row['precio']."</td>
               <td colspan='1'  style='text-align:center;'>".$row['descuento']."</td>
               <td colspan='2'  style='text-align:center;'>".$row['subtotal']."</td>
               </tr>";
  
    
}

$plantilla .= "</tbody>
        <tfoot>
        <tr><th>IVA</th></tr>";

foreach ($total_iva as $cuotas) {
    
    $plantilla .= "<tr><td>".$cuotas['tipo']."%"."</td><td>".$cuotas['importe']."</td></tr>";
    
                    
    
}
$plantilla .= "<tr><td> Total Iva ".$total_cuotas."</td><td> Total Detalle ".$total_detalle."</td> <td>Total Factura ".$total_factura."</td></tr>



        </tfoot></table>";

//        "
//            <table>
//           
//            <tr><td>Articulo</td><td>Descripción</td><td>Cantidad</td><td>Precio</td><td>Descuento</td><td>Subtotal</td></tr>
//            
//
//            <tr><td>I.V.A</td><td></td></td><td>Total Detalle</td><td></td><td>Total Factura</td><td></td></tr>
//            </table>
//            
//            
//
//
//    ";
    return $plantilla;    
        
}
?>  