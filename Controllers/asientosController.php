<?php
Class asientos_maestro extends Conexion{
    public $id;
    public $fecha;
    public $nasiento;
    public $titulo;
    
    public function getIdAsMaestro(){
        return $this->id;
    }
   public function getFechaAsiento(){
        return $this->fecha;
    }
    public function getAsiento(){
        return $this->asiento;
    }
    public function getTitulo(){
        return $this->titulo;
    }
    public function getDatosAsientoMaestro($asiento){
     $consulta = $this->conexion_db->prepare("SELECT * FROM asiento_contable_enca where asiento = $asiento");
     $consulta->execute(array($asiento));

     while($registro=$consulta->fetch(PDO::FETCH_ASSOC)){
         $this->id = $registro['id'];
         $this->fecha = $registro['fecha'];
         $this->asiento = $registro['asiento'];
         $this->titulo = $registro['titulo'];
     }
    }
    public function mostrarMaestroAsiento($asiento,$cliente,$FechaIni,$FechaFin){
    $filtro = " ";
    if ($asiento > 0 or $asiento != ""){
        $filtro = $filtro .  "AND am.asiento = $asiento";
    }

    if ($FechaIni > 0){
       $filtro = $filtro . "AND am.fecha between '$FechaIni' and '$FechaFin'";    

    }

    if ($cliente > 0 or $cliente != ""){
        $consulta = $this->conexion_db->prepare("SELECT am.id,am.fecha,am.asiento,am.titulo 
                                                       FROM asiento_contable_enca am 
                                                       join factura_maestro fm on fm.factura = am.asiento
                                                        where fm.id_cte = '$cliente' $filtro order by am.fecha desc");
    }else{

        $consulta = $this->conexion_db->prepare("SELECT * FROM asiento_contable_enca am where 1=1 $filtro");
    }    
        $consulta->execute();


    if($consulta){
//            $arr_detalle = array();
        $mihtmldetalle = "<table id='tabl_asientos' style='width:900px;line-height:1.5pt;'>";
        $mihtmldetalle .= "<thead><tr><th>Codigo</th><th>Fecha</th><th>Asiento</th><th>Titulo</th><th></th></tr></thead><tbody>";
        while($registro=$consulta->fetch(PDO::FETCH_ASSOC)){
            $id_asiento = $registro['id'];
            $fechaAsiento = $registro['fecha'];
            $nasiento = $registro['asiento'];
            $titulo = $registro['titulo'];

           $mihtmldetalle .= ("<tr onclick='mostrar_detalle_asiento()' style='font-size:15px;line-height:0.6pt;'><td>".$id_asiento."</td><td>".$fechaAsiento."</td><td>".$nasiento."</td><td>".$titulo."</td><td><img src='imagenes/blue_flechaRight.ico' style='width:45px;margin-top:-20px;cursor:pointer;' onclick='mostrar_asientos_detalle($nasiento)'></td></tr>");
           //otras formas de pasar el argumento como string .  \"$referencia_producto\"  -   \"{$referencia_producto}\" 


        }
//             echo json_encode($arr_detalle);
         $mihtmldetalle .= "</tbody></table>";
         echo $mihtmldetalle;

    } 

 }
   
}

Class asientos_detalle extends Conexion {
    public $id;
    public $nasiento;
    public $subcuenta;
    public $titulo;
    public $concepto;
    public $debe;
    public $haber;

	// modelo
    public function getIdAsDet() {
        return $this->id;
    }
    public function getNasiento() {
       return $this->nasiento;
    }
    public function getSubcuenta(){
        return $this->subcuenta;
    }
    public function getTitulo() {
        return $this->titulo;
    }
    public function getConcepto() {
       return $this->concepto;
    }
    public function getDebe() {
       return $this->debe;
    }
     public function getHaber() {
       return $this->haber;
    }
    public function setNasiento($asiento){
        $this->nasiento = $asiento;
    }
    
 
    
    
    public function getDatosAsientoDetalle($asiento){

        $consulta = $this->conexion_db->prepare("SELECT * FROM asiento_contable_det where nasiento = $asiento");
        $consulta->execute(array($asiento));
        
        while($registro=$consulta->fetch(PDO::FETCH_ASSOC)){
            $this->id = $registro['id'];
            $this->nasiento = $registro['nasiento'];
            $this->subcuenta = $registro['subcuenta'];
            $this->titulo = $registro['titulo'];
            $this->concepto = $registro['concepto'];
            $this->debe = $registro['debe'];
            $this->haber = $registro['haber'];
        }
     }   
   
     
   public function mostrarDetalleAsiento($asiento){
        
    $consulta_detalle = $this->conexion_db->prepare("SELECT * FROM asiento_contable_det where nasiento = $asiento");  
    $consulta_detalle->execute();


    if($consulta_detalle){
        $total_debe = 0;
        $total_haber = 0;
        $saldo = 0;
//            $arr_detalle = array();
        $mihtmldetalle = "<table class='' style='width:1000px;line-height:2pt;'><thead>";
        $mihtmldetalle .= "<tr><th>Codigo</th><th>Asiento</th><th>Subcuenta</th><th>Titulo</th><th>Concepto</th><th>Debe</th><th>Haber</th></tr></thead><tbody>";
        while($registro_detalle=$consulta_detalle->fetch(PDO::FETCH_ASSOC)){
            $id_asiento_det = $registro_detalle['id'];
            $Asiento_det = $registro_detalle['nasiento'];
            $subcuenta = $registro_detalle['subcuenta'];
            $titulo_det = $registro_detalle['titulo'];
            $concepto_det = $registro_detalle['concepto'];
            $debe = $registro_detalle['debe'];
            $haber = $registro_detalle['haber'];
            
          
           
           $mihtmldetalle .= ("<tr><td>".$id_asiento_det."</td><td>".$Asiento_det."</td><td>".$subcuenta."</td><td>".$titulo_det."</td><td>".$concepto_det."</td><td>".$debe."</td><td>".$haber."</td></tr>");
           //otras formas de pasar el argumento como string .  \"$referencia_producto\"  -   \"{$referencia_producto}\" 
            $total_debe = $total_debe + $debe;
            $total_haber = $total_haber + $haber;
            $saldo = $total_haber - $total_debe;
            $saldo = number_format($saldo, 5, '.', ',');

        }
//             echo json_encode($arr_detalle);
         $mihtmldetalle .= "</tbody><tfoot><tr><td></td><td></td><td></td><td></td><td></td><td>".$total_debe."</td><td>".$total_haber."</td><td>".$saldo."</td></tr></tfoot>";
         $mihtmldetalle .= "</table>";
         echo $mihtmldetalle;

    } 

 }
     
     
     
    public function GenerarAsientoFactura($factura,$total_factura,$fechaFactura,$cliente,$sumaCuotas){
      //buscamos el ultimo albaran guardado.  
        $datos_maestro = new facturaMaestro;
        $datos_maestro->getDatosMaestro($factura,'f');
        
//        $fechaAsiento =  $datos_maestro->getFechaFactura();
        $fechaAsiento =$fechaFactura;
//        $asiento = $datos_maestro->getfactura();
        $asiento = $factura;
        $titulo = 'Venta de Productos';
        $cutoasIva = $datos_maestro->getCuotaIva();
        
       
       
        //grabamos el encabezado del asiento de venta
        $consulta = $this->conexion_db->prepare("insert into asiento_contable_enca values('','$fechaAsiento',$asiento,'$titulo')");
        $consulta->execute();
        
        $totalFacturaConIva = $total_factura + $sumaCuotas; 
        

   
        //grabamos el detalle del asiento de venta
  
        
        $asiento_venta=array(
                    '0' => array(
                        "subcuenta"=>'700.0.00001',
                         "titulo"=>'Ventas de productos',
                         "concepto"=>'Venta de mercaderias',
                         "debe"=>'0',
                         "haber"=>$total_factura),
                     '1' => array(            
                        "subcuenta"=>'430.0.'. str_pad($cliente, 5, '0', STR_PAD_LEFT),
                        "titulo"=> 'Clientes, Público en general',
                        "concepto"=>'Venta de mercaderias',
                        "debe"=>$totalFacturaConIva,
                         "haber"=>'0')
                   );   
                   
        $i=0;
       // var_dump($asiento_venta);
//        echo"<pre>";
//        var_dump($asiento_venta);
//        echo"</pre>";
         foreach($cutoasIva as $row){
             
             $asiento_cuota=array(
                            "subcuenta"=> '477.0.'. str_pad($row['tipo'], 5, '0', STR_PAD_LEFT),
                            "titulo"=> 'HP IVA repercutido '.$row['tipo'].'%',
                            "concepto"=> 'Venta de productos',
                            "debe"=>'0',
                            "haber"=> $row['importe']);
             
              array_push($asiento_venta, $asiento_cuota);

            $i++;
        }

        
       $i=0;
       foreach($asiento_venta as $row){
//            echo "<pre>";
//            var_dump($asiento_venta[$i]['subcuenta'],
//                    $asiento_venta[$i]['titulo'],
//                    $asiento_venta[$i]['concepto'],
//                    $asiento_venta[$i]['debe'],
//                    $asiento_venta[$i]['haber']);
//            echo "</pre>";
           $subcuenta = $asiento_venta[$i]['subcuenta'];
           $titulo = $asiento_venta[$i]['titulo'];
           $concepto = $asiento_venta[$i]['concepto'];
           $debe = $asiento_venta[$i]['debe'];
           $haber = $asiento_venta[$i]['haber'];
            
            $consulta_asDet = $this->conexion_db->prepare("INSERT INTO asiento_contable_det VALUES('','$factura','$subcuenta','$titulo','$concepto','$debe','$haber')");
          
            $consulta_asDet->execute();
            
            $i++;
       }

     }
     
     
    public function introducir_asiento($subcuenta,$titulo,$concepto,$debe,$haber){
      
        $asiento_det = new asientos_detalle;
        echo $asiento_det->nasiento;
       
        
    } 
     
  
}

