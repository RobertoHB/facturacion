<?php

  //  require 'Controllers/conexion.php';

Class productos extends Conexion {
    
    public $id;
    public $nombre;
    public $descripcion;
    public $iva;
    public $tarifa;
    
    public $lista;

	// modelo
    public function getId() {
        return $this->id;
    }
    public function getNombre() {
       return $this->nombre;
    }
    public function getDescripcion() {
       return $this->descripcion;
    }
    public function getIva() {
       return $this->iva;
    }
    public function getTarifa() {
       return $this->tarifa;
    }

    
    public function setNombre($nombre) {
       $this->nombre = $nombre;
    }
    public function setDescripcion($decripcion) {
       $this->descripcion = $decripcion;
    }
      public function setIva($iva) {
       $this->iva = $iva;
    }
      public function setTarifa($tarifa) {
       $this->tarifa = $tarifa;
    }

        
     public function getDatosProducto($nombre){
      
        //$sql = SELECT * FROM clientes where id = '$id'";
        $consulta = $this->conexion_db->prepare("SELECT * FROM productos where nombre = '$nombre'");
        //$consulta->bindParam(':id', $id);
        $consulta->execute(array($nombre));
        
        while($registro=$consulta->fetch(PDO::FETCH_ASSOC)){
            $this->id = $registro['id'];
            $this->nombre = $registro['nombre'];
            $this->descripcion = $registro['descripcion'];
            $this->iva = $registro['iva'];
            $this->tarifa = $registro['tarifa'];
        
        } 
       
     }
             
     public function getDatosProducto_id($id){
      
        //$sql = SELECT * FROM clientes where id = '$id'";
        $consulta = $this->conexion_db->prepare("SELECT * FROM productos where id = '$id'");
        //$consulta->bindParam(':id', $id);
        $consulta->execute(array($id));
        if($consulta){
            
        
            while($registro=$consulta->fetch(PDO::FETCH_ASSOC)){
                $this->id = $registro['id'];
                $this->nombre = $registro['nombre'];
                $this->descripcion = $registro['descripcion'];
                $this->iva = $registro['iva'];
                $this->tarifa = $registro['tarifa'];
            } 
        }
     }
     
     public function getSelectProductos(){
      
        
        $consulta = $this->conexion_db->prepare("SELECT id,nombre FROM productos");
        //$consulta->bindParam(':id', $id);
        $consulta->execute();
//        $lista .= "<option value=''></option>";
         $array = array();
        while($registro=$consulta->fetch(PDO::FETCH_ASSOC)){
//           $selected = 'selected="selected"';
          $nombre = utf8_encode($registro['nombre']);
          array_push($array, $nombre);
        } 
       echo json_decode($array);
      
     }
     
    
}
