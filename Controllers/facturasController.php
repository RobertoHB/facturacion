<?php

Class facturaMaestro extends clientes{
    public $id;
    public $id_cte;
    public $factura;
    public $cuotaiva = array();
    public $total;
    public $fecha;
    public $fecha_factura;

	// modelo
    public function getId() {
        return $this->id;
    }
    public function getidCliente() {
       return $this->id_cte;
    }
    public function getfactura(){
        return $this->factura;
    }
    public function getCuotaIva() {
        return $this->cuotaiva;
    }
    public function getTotal() {
       return $this->total;
    }
    public function getFecha() {
       return $this->fecha;
    }
     public function getFechaFactura() {
       return $this->fecha_factura;
    }

    public function getVaciarArrayCuotas(){
        for ($i=0; $i<count($cuotaiva); $i++){
            $this->cuotaiva[$i]='';
        }
    }
        
    public function getSumaCuotasFactura(){
        //$factura =$this->getfactura();
        $cuotas = $this->getCuotaIva();
        $sumaCuotas = 0;
        foreach($cuotas as $row){   
            $SumaCuotas = $SumaCuotas + $row['importe'];    
        }

        return $SumaCuotas;
    }
    
    
     public function getDatosMaestro($factura,$tipo){

        //$sql = SELECT * FROM clientes where id = '$id'";
         // id corresponde al numero de albaran generado y factura es el numero de factura ya definitivo
         //comprobamos si la busqueda es por albaran o por factura
         if($tipo === 'f'){
             $consulta = $this->conexion_db->prepare("SELECT * FROM factura_maestro where factura = $factura");
         }else{    
             $consulta = $this->conexion_db->prepare("SELECT * FROM factura_maestro where id = $factura");
         }
         
        //$consulta = $this->conexion_db->prepare("SELECT * FROM factura_maestro where id = $factura or factura = $factura");
        //$consulta->bindParam(':id', $id);
        $consulta->execute(array($factura));
        
        while($registro=$consulta->fetch(PDO::FETCH_ASSOC)){
            $this->id = $registro['id'];
            $this->id_cte = $registro['id_cte'];
            $this->factura = $registro['factura'];
            $this->total = $registro['total'];
            $this->fecha = $registro['fecha'];
            $this->fecha_factura = $registro['fecha_factura'];
        }
        
         
        if ($this->factura >0){
             $consultaIva = $this->conexion_db->prepare("SELECT porc,importe FROM cuotasiva join tiposiva on cuotasiva.tipo = tiposiva.id where cuotasiva.id = $this->factura");
             $consultaIva->execute(array($this->factura));
        }else{
          
            $consultaIva = $this->conexion_db->prepare("SELECT porc,importe FROM cuotasiva join tiposiva on cuotasiva.tipo = tiposiva.id where cuotasiva.id = $factura");
            $consultaIva->execute(array($factura));
        }
        //cargamos las cuotas de iva de los datos de factura que consultamos
      
        
        $i=0;
        while($registroiva=$consultaIva->fetch(PDO::FETCH_ASSOC)){
        
            $this->cuotaiva[$i]['tipo'] = $registroiva['porc'];
            $this->cuotaiva[$i]['importe'] = $registroiva['importe'];
            $i++;
        }
     
     }          



     public function GenerarFacturaNueva(){
      //buscamos el ultimo albaran guardado.  
      $consulta = $this->conexion_db->prepare("SELECT MAX(id)ultimaFact from factura_maestro");
      $consulta->execute();
        
        while($registro=$consulta->fetch(PDO::FETCH_ASSOC)){
            $this->factura = $registro['ultimaFact']+1;
        }
     }
     
    public function GrabarFactura($albaran){
          $fecha_ahora = date('Y-m-d');
        
          $consulta = $this->conexion_db->prepare("SELECT MAX(factura)nuevafactura from factura_maestro");
          $consulta->execute();
          $nuevaFactura = $consulta->fetch(PDO::FETCH_ASSOC);
          
        
          //si no existe todavia ninguna factura a numero 1 de lo contrario la ultima + 1
          $nFactura= $nuevaFactura['nuevafactura'] + 1;
          
          
          
          $consulta = $this->conexion_db->prepare("UPDATE factura_maestro SET factura = $nFactura, fecha_factura = '$fecha_ahora' WHERE id = '$albaran'");
          $consulta->execute();   
          //actualizamos tambien en la tabla cuotasiva el numero de albaran. Ahora corresponderá al numero de factura
          $consulta1 = $this->conexion_db->prepare("UPDATE cuotasiva SET id = $nFactura WHERE id = '$albaran'");
          $consulta1->execute();
        
        
       
    }
         
     
    
    
    
     
     
    public function guardaCabeceraCliente($id_cte,$fecha_factura,$factura){
          $factura = trim($factura);
         //comprobamos si ya está facturado para imposibilitar su modificación. 
        

       //comprobamos si la factura a guardar ya existe o es una nueva.    
       $consulta = $this->conexion_db->prepare("SELECT id FROM factura_maestro WHERE id = $factura");
//       $consulta->bindParam(':factura', $factura, PDO::PARAM_INT);
       $consulta->execute();
       $registros = $consulta->rowCount();
       
       if ($registros == 0){
           
            $consulta = $this->conexion_db->prepare("insert into factura_maestro values('',$id_cte,'0','0','0','$fecha_factura','0')");
       }else{
         
            $consulta = $this->conexion_db->prepare("UPDATE factura_maestro SET id_cte = $id_cte, fecha = '$fecha_factura' WHERE id = $factura");
       }
       
       $consulta->execute();
       
       
  
     }
     
     public function comprobarFacturado($factura){
       $factura = trim($factura);
       //comprobamos si la factura proforma ya esta facturada   
       $consulta = $this->conexion_db->prepare("SELECT factura FROM factura_maestro WHERE id = '$factura'");
       $consulta->execute();
       $registros = $consulta->fetch(PDO::FETCH_ASSOC);
     
        if (($registros['factura'] != "") and ($registros['factura'] > 0)){
            $factura_n = $registros['factura'];
            echo ("Albaran ya Facturado. No se puede modificar.".$factura_n);
            exit;
        }
     }
     
}

Class facturaDetalle extends productos{
    
  
     
     public function mostrarDetalleFactura($factura,$tipo){
         
         //Si la busqueda es por factura sacamos el albaran al que corrsponde y hacemos la consulta
         if ($tipo === 'f'){
              $consulta = $this->conexion_db->prepare("SELECT * FROM factura_detalle d join factura_maestro m on d.id_factura = m.id where m.factura = '$factura'");
         }else{
              $consulta = $this->conexion_db->prepare("SELECT * FROM factura_detalle where id_factura = '$factura'");
         }
        //$consulta = $this->conexion_db->prepare("SELECT * FROM factura_detalle where id_factura = '$factura'");
        $consulta->execute();
         
        if($consulta){
//            $arr_detalle = array();
            $mihtmldetalle = "<table style='width:1100px;'>";
            $mihtmldetalle .= "<tr><td>Articulo</td><td>Descripcion</td><td>Cantidad</td><td>Precio</td><td>Descuento</td><td>Subtotal</td><td></td><td></td>";
            while($registro=$consulta->fetch(PDO::FETCH_ASSOC)){
                $id_detalle = $registro['id'];
                $id_prod = $registro['id_producto'];
                $cantidad = $registro['cantidad'];
                $descuento = $registro['descuento'];
                $precio = $registro['precio'];
                $subtotal = $registro['subtotal'];
                
              

                $datos_producto = new productos;
                
                $datos_producto->getDatosProducto_id($id_prod);

                $referencia_producto =$datos_producto->getNombre();
                $descripcion_producto = $datos_producto->getDescripcion();
                //$precio_producto = $datos_producto->getTarifa();
                
                $datos_maestro = new facturaMaestro;
                $datos_maestro->getDatosMaestro($factura,$tipo);
                
                $totaldetalle = $datos_maestro->getTotal();
                
                 //$arr_detalle[] =array('producto'=>$referencia_producto,'descripcion'=>$descripcion_producto,'cantidad'=>$cantidad,'precio'=>$precio,'descuento'=>$descuento,'subtotal'=>$subtotal);
              
               $mihtmldetalle .= ("<tr><td>".$referencia_producto."</td><td>".$descripcion_producto."</td><td>".$cantidad."</td><td>".$precio."</td><td>".$descuento."</td><td>".$subtotal."</td><td><img src='imagenes/papelera.png' class='deshab' style='width:20px;cursor:pointer' onclick='eliminar_detalle($id_detalle)'></td><td><img src='imagenes/editar.png' class='deshab' style='width:20px;cursor:pointer' onclick='editar_detalle($id_detalle,".'"'.$referencia_producto.'"'.",$cantidad,$descuento,$precio)'></td><td id='iva' style='display:none'></td><td id='totaldet' style='display:none'>".$totaldetalle."</td></tr>");
               //otras formas de pasar el argumento como string .  \"$referencia_producto\"  -   \"{$referencia_producto}\" 
               
               
            }
//             echo json_encode($arr_detalle);
             $mihtmldetalle .= "</table>";
             echo $mihtmldetalle;
           
        } 
         
     }
     
     public function guardarDetalleFactura($id_factura,$nom_producto,$cant,$dto,$id_detalle,$precio_actualizar){
         $id_producto = new productos();
         $codigo_producto = $id_producto->getDatosProducto($nom_producto);
         $codigo_producto = $id_producto->getId();
         
         $precio_producto = $id_producto->getTarifa();
         $subtotal_sdto = ($precio_producto * $cant);
         $subtotal = $subtotal_sdto - ($subtotal_sdto*$dto/100);
         
         if ($id_detalle == 0){
             
            $consulta = $this->conexion_db->prepare("insert into factura_detalle values('','$id_factura','$codigo_producto','$cant','$precio_producto','$dto','$subtotal')");  
         }else{
            $subtotal_sdto = ($precio_actualizar * $cant);
            $subtotal = $subtotal_sdto - ($subtotal_sdto*$dto/100);
          
            $consulta = $this->conexion_db->prepare("UPDATE factura_detalle SET id_producto = '$codigo_producto',cantidad='$cant',precio='$precio_actualizar',descuento='$dto',subtotal='$subtotal' WHERE id = '$id_detalle'");     
         }
         
       
       // $consulta->bindParam(':id', $id);
       
        $consulta->execute();
        
        
        
//        mostrarDetalleFactura($id_factura);
       //$mostrando_detalle = $this->mostrarDetalleFactura($id_factura);

     }
     
     public function elimina_detalle($id_detalle){
         
         $consulta = $this->conexion_db->prepare("DELETE FROM factura_detalle WHERE id = $id_detalle");
         $consulta->execute();     
         
     }
     
     
      public function mostrarDetallepdf($factura){
         
        $consulta = $this->conexion_db->prepare("SELECT * FROM factura_maestro m join factura_detalle d on m.id = d.id_factura where m.factura = '$factura'");
        
        //$consulta = $this->conexion_db->prepare("SELECT * FROM factura_detalle where id_factura = '$factura'");
        $consulta->execute();
        
        if($consulta){
            while($registro=$consulta->fetch(PDO::FETCH_ASSOC)){
                $id_detalle = $registro['id'];
                $id_prod = $registro['id_producto'];
                $cantidad = $registro['cantidad'];
                $descuento = $registro['descuento'];
                $precio = $registro['precio'];
                $subtotal = $registro['subtotal'];
                
                $datos_producto = new productos;
                
                $datos_producto->getDatosProducto_id($id_prod);

                $referencia_producto =$datos_producto->getNombre();
                $descripcion_producto = $datos_producto->getDescripcion();
               
                
//                $datos_maestro = new facturaMaestro;
//                $datos_maestro->getDatosMaestro($factura,$tipo);
                
                //$totaldetalle = $datos_maestro->getTotal();
                
                $arr_detalle[] =array('producto'=>$referencia_producto,'descripcion'=>$descripcion_producto,'cantidad'=>$cantidad,'precio'=>$precio,'descuento'=>$descuento,'subtotal'=>$subtotal);
              
               //$mihtmldetalle .= ("<tr><td>".$referencia_producto."</td><td>".$descripcion_producto."</td><td>".$cantidad."</td><td>".$precio."</td><td>".$descuento."</td><td>".$subtotal."</td><td>");
               //otras formas de pasar el argumento como string .  \"$referencia_producto\"  -   \"{$referencia_producto}\" 
               
               
            }
            return $arr_detalle;
             //echo json_encode($arr_detalle);
//             $mihtmldetalle .= "</table>";
//             echo $mihtmldetalle;
           
        } 
         
     }
     
     
     
     
     
    
}

